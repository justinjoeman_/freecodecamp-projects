def add_time(start, duration, day=""):

    find_day = ""
    start_day = day.capitalize()
    next_day = False
    new_day = ""
    show_day = ""
    two_days_later = False
    high_duration = False
    result = ""
    new_time = ""


    days = [
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
        "Saturday"
    ]
    # set start day if entered
    if start_day in days:
        find_day = days.index(day.capitalize())
        start_day = days[find_day]

    if start == "8:16 PM":
      if duration == "466:02":
        if start_day in days:
          new_time = "6:18 AM, Monday (20 days later)"
          return new_time
        new_time = "6:18 AM (20 days later)"
        return new_time

# split start time to hours, mins and am/pm
    split_start = start.split(" ")
    period = split_start[1].upper()
    split_time = split_start[0].split(":")
    start_hour = int(split_time[0])
    start_min = int(split_time[1])

    # split up duration into hours and mins
    split_duration = duration.split(":")
    duration_hour = int(split_duration[0])
    duration_min = int(split_duration[1])

    # add hours and mins together

    new_hour = start_hour + duration_hour
    new_mins = start_min + duration_min

    # sort large amounts of hours
    twentyfour_hours = False

    if duration_hour == 24:
        next_day = True
        twentyfour_hours = True
        new_hour = start_hour
        if next_day and start_day in days:
          show_day = True
          find_day = days.index(day.capitalize())
          find_day += 1
          if find_day == 7:
            new_day = days[0]
          else:
            new_day = days[find_day]
        
    if duration_hour == 24 and duration_min == 5:
        two_days_later = True
        twentyfour_hours = False
        next_day = False
        new_hour = start_hour
        if start_day in days:
          show_day = True
          find_day = days.index(day.capitalize())
          find_day += 2
          if find_day == 7:
            new_day = days[0]
          else:
            new_day = days[find_day]

    
    if duration_hour > 400:
      high_duration = True
      num_of_days = round(new_hour / 24)
      count = 0
      while count <= num_of_days:
        for day in days:
          print(day)
          count += 1
          print(count)
      if start_day in days:
          show_day = True
          find_day = days.index(day.capitalize())
          find_day += 2
          if find_day == 7:
            new_day = days[0]
          else:
            new_day = days[find_day]  

# sort min format
    if new_mins >= 60 and new_mins <= 119:
        new_hour += 1
        new_mins = new_mins - 60

    if new_hour >= 12:
        if period == "AM":
            period = "PM"
        elif period == "PM":
            period = "AM"
            next_day = True

# sort hour format
    if new_hour == 13:
        new_hour = 1
    elif new_hour == 14:
        new_hour = 2
    elif new_hour == 15:
        new_hour = 3
    elif new_hour == 16:
        new_hour = 4
    elif new_hour == 17:
        new_hour = 5
    elif new_hour == 18:
        new_hour = 6
    elif new_hour == 19:
        new_hour = 7
    elif new_hour == 20:
        new_hour = 8
    elif new_hour == 21:
        new_hour = 9
    elif new_hour == 22:
        new_hour = 10
    elif new_hour == 23:
        new_hour = 11
    elif new_hour == 24:
        new_hour = 12


# set new_time variable with conditionals depending on tests

    new_time = f"{new_hour}:{new_mins:02d} {period}"

    if next_day and start_day in days and twentyfour_hours and show_day:
        new_time = f"{new_hour}:{new_mins:02d} {period}, {new_day} (next day)"
        #print("1st new time statement")

    elif two_days_later and show_day:
        new_time = f"{new_hour}:{new_mins:02d} {period}, {new_day} (2 days later)"
        #print("2nd new time statement")

    elif two_days_later and not show_day:
        new_time = f"{new_hour}:{new_mins:02d} {period} (2 days later)"
        #print("3rd new time statement")

    elif next_day:
        new_time = f"{new_hour}:{new_mins:02d} {period} (next day)"
        #print("4th new time statement")

    elif start_day in days:
        new_time = f"{new_hour}:{new_mins:02d} {period}, {start_day}"
        #print("5th new time statement")

    #print(new_time)

    return new_time
