def arithmetic_arranger(problems, show_answer=False):
  
  arranged_problems = ""
  line1 = ""
  line2 = ""
  line3 = ""
  line4 = ""

  if len(problems) > 5:
    err1 = "Error: Too many problems."
    return err1

# split string into 3 parts and process on each part + validate operators

  for problem in problems:
    items = problem.split(" ")

    if items[1] == "/" or items[1] == "*":
      err2 = "Error: Operator must be '+' or '-'."
      return err2

    line_count = "x"

# workout length of dashes needed + validate digits
    
    numbers = [items[0], items[2]]

    for num in numbers:

      if not num.isnumeric():
        err3 = "Error: Numbers must only contain digits."
        return err3
        
      if len(num) > 4:
        err4 = "Error: Numbers cannot be more than four digits."
        return err4

      if len(num) > len(line_count):
        line_count = "x" * len(num)

    dash_line = "-" * (len(line_count) + 2)

# work out formatting of each number 

    num1 = ""  
    num2 = ""

# first number

    if len(dash_line) == 3:
      num1 = f"{items[0].rjust(3)}"

    if len(dash_line) == 4:
      num1 = f"{items[0].rjust(4)}"

    if len(dash_line) == 5:
      num1 = f"{items[0].rjust(5)}"
    
    if len(dash_line) == 6:
      num1 = f"{items[0].rjust(6)}"

# second number
    
    if len(dash_line) == 3:
      num2 = f"{items[1]}{items[2].rjust(2)}"

    if len(dash_line) == 4:
      num2 = f"{items[1]}{items[2].rjust(3)}"

    if len(dash_line) == 5:
      num2 = f"{items[1]}{items[2].rjust(4)}"
    
    if len(dash_line) == 6:
      num2 = f"{items[1]}{items[2].rjust(5)}"

# do math to get answer

    a = int(items[0])
    b = int(items[2])
    if items[1] == "+":
      answer = a + b
    elif items[1] == "-":
      answer = a - b

    answer = str(answer)
      
    if len(dash_line) == 4:
      solution_string = f"{answer.rjust(4)}"

    if len(dash_line) == 5:
      solution_string = f"{answer.rjust(5)}"

    if len(dash_line) == 6:
      solution_string = f"{answer.rjust(6)}"

# add to relevant line

    line1 += num1 + "    " 
    line2 += num2 + "    " 
    line3 += dash_line + "    " 
    line4 += solution_string + "    "


  if show_answer == True:
    arranged_problems = f"{line1.rstrip()}\n{line2.rstrip()}\n{line3.rstrip()}\n{line4.rstrip()}"
  
  if show_answer == False:
    arranged_problems = f"{line1.rstrip()}\n{line2.rstrip()}\n{line3.rstrip()}"
  
  #print(f"printing final output:\n{arranged_problems}")
  #print(arranged_problems)

  return arranged_problems
