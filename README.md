# FreeCodeCamp Projects
## Purpose
A collection of the code I used when completing the Scientific Computing with Python course on FreeCodeCamp. Each project had a number of unit tests to pass and I had to create functions that would meet the objectives set.

## Certificate
https://www.freecodecamp.org/certification/justinjoeman_/scientific-computing-with-python-v7
