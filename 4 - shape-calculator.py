class Rectangle:

  def __init__(self, width, height):
    self.width = width
    self.height = height

  def __str__(self):
    return f'Rectangle(width={self.width}, height={self.height})'

  def set_height(self, height):
    self.height = height

  def set_width(self, width):
    self.width = width
  
  def get_area(self):
    return self.width * self.height

  def get_perimeter(self):
    return (2 * self.width) + (2 * self.height)

  def get_diagonal(self):
    return (self.width ** 2 + self.height ** 2) ** .5

  def get_picture(self):
    stars_per_line = self.width
    num_of_lines = self.height
    shape = ""
    if stars_per_line >= 50 or num_of_lines >= 50:
      return "Too big for picture."
    else:
      for line in range(num_of_lines):
        shape += '*' * stars_per_line + '\n'
    print(shape)
    return shape

  def get_amount_inside(self, shape):
    new_shape = shape.get_area()
    old_shape = self.get_area()
    i = 0
    while old_shape >= new_shape:
      old_shape = old_shape - new_shape
      i=i+1
    return i  


class Square(Rectangle):

  def __init__(self, length):
    self.width = length
    self.height = length

  def __str__(self):
    return f'Square(side={self.width})'

  def set_side(self, side):
    self.width = side
    self.height = side

  def set_height(self, height):
    self.height = height
    self.width = height

  def set_width(self, width):
    self.width = width
    self.height = width
