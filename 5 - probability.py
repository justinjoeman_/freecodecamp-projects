import copy
import random
# Consider using the modules imported above.

class Hat:
    
    def __init__(self, **kwargs):

        self.contents = []
        # cycle through each ball colour to add to contents
        for ball_colour in kwargs:
            self.contents = self.contents + [ball_colour]*kwargs[ball_colour]
            #print(self.contents)
        
    
    def draw(self, no_balls):
        drawn = []
        if len(self.contents) <= no_balls:
            return self.contents
        else:
            for item in range(no_balls):
              # pick a random choice
                selection = random.choice(self.contents)
                self.contents.remove(selection)
                drawn.append(selection)
                #print(drawn)
            return drawn



def experiment(hat, expected_balls, num_balls_drawn, num_experiments):
  
  expected_ball = []
  success = 0

  for k,v in expected_balls.items():
    for item in range(v):
      expected_ball.append(k)
  
  for draw in range(num_experiments):
    hat_copy = copy.deepcopy(hat)
    choice_list = hat_copy.draw(num_balls_drawn)
    picked = False
    for item in expected_ball:
      #print(f'in draw loop: {item}')
      if item in choice_list:
        picked = True
        choice_list.remove(item)
        #print(f'removing item from choice list: {item}\nchoice list now: {choice_list}')
      else:
        picked = False
        break
    if picked:
      success += 1
  
  prob = success / num_experiments
  return prob
  
