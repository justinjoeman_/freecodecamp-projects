def add_time(start, duration, day = "no_day"):

    # split up data received

    start_hour = int(start.split(":")[0])

    if "PM" in start: # 24 hour clock change
        start_hour += 12

    start_min = int(start.split(":")[1].split()[0])

    duration_hour = int(duration.split(":")[0])
    duration_min = int(duration.split(":")[1])
    
    # work out how many days might have passed

    days_later = int((start_hour + duration_hour + int((start_min + duration_min) / 60)) / 24) 

    # work out the hour with time period

    end_time_hour = (start_hour + duration_hour + int((start_min + duration_min) / 60)) % 24 
    PM = False  

    if end_time_hour >= 12:        
        end_time_hour -= 12
        if end_time_hour == 0:
            end_time_hour = 12
        PM = True

    if end_time_hour == 0:
            end_time_hour = 12

    end_time_min = str((start_min + duration_min) % 60).zfill(2)

    if day == "no_day":  
    
        if days_later > 1:
            if PM:
                new_time = f"{end_time_hour}:{end_time_min} PM ({days_later} days later)"
            else:
                new_time = f"{end_time_hour}:{end_time_min} AM ({days_later} days later)"
        elif days_later == 1:
            if PM:
                new_time = f"{end_time_hour}:{end_time_min} PM (next day)"
            else:
                new_time = f"{end_time_hour}:{end_time_min} AM (next day)"
        else:
            if PM:
                new_time = f"{end_time_hour}:{end_time_min} PM"
            else:
                new_time = f"{end_time_hour}:{end_time_min} AM"
    
    else: 
        days = ("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday") 

        # work out the day

        day = days[(days.index(day.capitalize()) + 1 + days_later) % 7 - 1] 
    
        if days_later > 1:
            if PM:
                new_time = f"{end_time_hour}:{end_time_min} PM, {day} ({days_later} days later)"
            else:
                new_time = f"{end_time_hour}:{end_time_min} AM, {day} ({days_later} days later)"
        elif days_later == 1:
            if PM:
                new_time = f"{end_time_hour}:{end_time_min} PM, {day} (next day)"
            else:
                new_time = f"{end_time_hour}:{end_time_min} AM, {day} (next day)"
        else:
            if PM:
                new_time = f"{end_time_hour}:{end_time_min} PM, {day}"
            else:
                new_time = f"{end_time_hour}:{end_time_min} AM, {day}"

    return new_time
