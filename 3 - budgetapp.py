class Category:

    def __init__(self, name):
        self.name = name
        self.ledger = []


    def __str__(self):
        output = ""
        total = 0
        title = self.name.center(30, "*")
        output += f'{title}\n'
        for transaction in self.ledger:
          line = str(transaction['description'][:23]).ljust(23) + str("{:.2f}".format(transaction['amount']).rjust(7) + "\n")
          output += line
          total = total + float(transaction['amount'])
        output += f'Total: {total}'

        return output


        #return f'{self.name}'


    def deposit(self, amount, description=""):
        if description == "":
            data = {'amount': amount, 'description': ''}
            #print(f"Deposit function called\n{data}")
            self.ledger.append(data)
        else:
            data = {'amount': amount, 'description': description}
            #print(f"Deposit function called\n{data}")
            self.ledger.append(data)

    def withdraw(self, amount, description=""):
        approve_transaction = self.check_funds(amount)
        # balance = self.ledger[0]["amount"]
        if description == "":
            data = {'amount': -amount, 'description': ""}
            if not approve_transaction:
                #print(f"Not enough funds to withdraw\n{data}")
                return False
            elif approve_transaction:
                #print(f"Withdrawing funds...\n{data}")
                self.ledger.append(data)
                return True
        else:
            data = {'amount': -amount, 'description': description}
            if not approve_transaction:
                #print(f"Not enough funds to withdraw\n{data}")
                return False
            elif approve_transaction:
                #print(f"Withdrawing funds...\n{data}")
                self.ledger.append(data)
                return True

    def get_balance(self):
        current_balance = 0
        for transaction in self.ledger:
            current_balance = current_balance + transaction["amount"]
        return current_balance


    def transfer(self, amount, category):
        approve_transaction = self.check_funds(amount)
        data = {'amount': -amount, 'description': f"Transfer to {category.name}"}
        #print(f'printing data: {data}')
        if not approve_transaction:
            #print("Not enough funds to transfer to destination account")
            return False
        elif approve_transaction:
            #print(f"Transferring to {category}")
            self.ledger.append(data)
            new_data = {'amount': amount, 'description': f"Transfer from {self.name}"}
            #print(f'printing NEW data: {data}')
            category.ledger.append(new_data)
            return True



    def check_funds(self, amount):
        current_balance = self.get_balance()
        #print(f"Checking funds... \nCurrent balance: {current_balance}\nAmount: {amount}")
        if amount > current_balance:
            #print("Returning False")
            return False
        else:
            #print("Returning True")
            return True

    
def round_number(number):
    if number < 10:
      return 0
    else:
      return round(number / 10.0) * 10


def create_spend_chart(categories):
  deductions = []

  # try and find the max length category
  
  max_length_category = 0
  count = 0

  for item in categories:

    withdraw_amount = 0

    for entry in item.ledger:

      if entry["amount"] < 0:
        withdraw_amount += -entry["amount"]
        count += (-entry["amount"])
        
    # finding the category with the max length

    if len(item.name) > max_length_category:
      max_length_category = len(item.name)
    deductions.append([item.name,withdraw_amount])
  
  # work out percentage in category

  for item in deductions:
    item.append(round_number((item[1]/count)*100))
  count = ""
  count += "Percentage spent by category\n"
  total = 100

  while total >= 0:
    count += str(total).rjust(3) + "|" + " "

    for item in range(len(deductions)):
      if deductions[item][2] >= total:
        count += "o" + "  "
      else:
        count += "   "
    total -= 10
    count += "\n"

  # format line with ----

  count += "    " + ("-"*10) + "\n"

  loop_var = 0

  for item in range(max_length_category):
    count += "     "
    for entry in range(len(deductions)):
      # check if character exists
      if len(deductions[entry][0]) -1 < loop_var:
        count += "   "
      else:
        count += deductions[entry][0][loop_var] + "  "
    loop_var += 1
    if item != max_length_category -1:
      count += "\n"


  return count
  
